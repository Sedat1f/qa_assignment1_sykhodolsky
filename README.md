To get into the shell of server container:
~~~~
docker exec -it server bash
cd serverdata
ls
~~~~

To get into the shell of client container:
~~~~
docker exec -it client bash
cd clientdata
ls
~~~~