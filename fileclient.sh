echo "Getting server IP..."
IP=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' server)
echo "Building image..."
docker build client -t testclient --build-arg "PORT=$1" --build-arg "IP=$IP"
echo $1
echo $IP
echo "Creating volume..."
docker volume create clientvol
echo "Mounting volume..."
docker run -it --rm -d -v clientvol:/clientdata --name client --net testnetwork testclient

$SHELL