import socket
import sys
import string    
import random
import hashlib

serv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
HOST = socket.gethostname()
DNS_ADDR = socket.gethostbyname(HOST)
PORT = int(str(sys.argv[len(sys.argv) - 1]))
serv.bind((DNS_ADDR, PORT))
serv.listen(5)
FILEPATH = "/serverdata/test"

def get_checksum(filepath):
    md5_hash = hashlib.md5()
    with open(filepath, "rb") as a_file:
        content = a_file.read()
        md5_hash.update(content)
    return md5_hash.hexdigest()

def generate_file(filepath):
    S = 1024
    ran = ''.join(random.choices(string.ascii_letters + string.digits, k = S))
    with open(filepath, "w+") as file:
        file.write(str(ran))

generate_file(FILEPATH)
digest = get_checksum(FILEPATH)
f = open(FILEPATH, "r")
data = f.read()

while True:
    conn, addr = serv.accept()
    print(f"connection from {addr}")
    conn.send(data.encode("utf-8"))
    conn.send(digest.encode("utf-8"))
    f.close()