echo "Creating network..."
docker network create testnetwork
echo "Building image..."
docker build server -t testserver --build-arg "PORT=$1"
echo "Creating volume..."
docker volume create servervol
echo "Mounting volume and container..."
docker run -it --rm -d -v servervol:/serverdata --name server --net testnetwork testserver

$SHELL