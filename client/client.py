import socket
import sys
import hashlib

serv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
IP = str(sys.argv[len(sys.argv) - 1])
PORT = int(str(sys.argv[len(sys.argv) - 2]))
print(IP)
print(PORT)
serv.connect((IP, PORT))
FILEPATH = "/clientdata/test"

def get_checksum(filepath):
    md5_hash = hashlib.md5()
    with open(filepath, "rb") as a_file:
        content = a_file.read()
        md5_hash.update(content)
    return md5_hash.hexdigest()

file_for_checksum = False

while True:
    data = serv.recv(1024)
    if len(data) == 1024:
        file_for_checksum = True
        with open(FILEPATH, "w+") as file:
            file.write(data.decode())
            file.close()
            print("file data is correct")
    elif len(data) < 1024:
        if file_for_checksum == True:
            checksum = data.decode("utf-8")
            real_checksum = get_checksum(FILEPATH)
            if checksum == real_checksum:
                print("checksum is correct")